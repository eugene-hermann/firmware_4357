#include "LPC17xx.h"


#define PIN_9 0x0200

int main( void ) {
  LPC_PINCON->PINSEL0 = 0;
  LPC_GPIO0->FIODIR = (1 << 9);

  register uint32_t pin9 = PIN_9;
  volatile uint32_t *clr = &(LPC_GPIO0->FIOCLR);
  volatile uint32_t *set = &(LPC_GPIO0->FIOSET);
//  for (;;);
//  0000017e:   str     r3, [r1, #0]
//  00000180:   str     r3, [r2, #0]
//  00000182:   b.n     0x17e <main+22>

//  asm ("X:"
//      : "str %1, %0\n\t"
//      : "str %2, %0"
//      : "=r" (clr)
//      : "=r" (set)
//      :  "b X"
//);
	for( ;; ) {
//	  LPC_GPIO0->FIOCLR = PIN_9;
//    LPC_GPIO0->FIOSET = PIN_9;
	  *clr = pin9;

	  *set = pin9;
	}
	return 0;
}
