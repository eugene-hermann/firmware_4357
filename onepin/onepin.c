/*
  1 wire communication protocol implementation.
  This file is part of 'UCSCx, 4357.(010) Embedded Firmware Essentials' project.

  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "led.h"
#include "onepin.h"

void __error__(void){
  for(;;) {}
}

// Cross platform protocol implementation.

static inline void busy_loop(uint32_t count) {
  volatile uint32_t i;
  for (i = count; i != 0; i--) {};
}

static inline bool wait_1wire_high(uint32_t *timeout) {
  for (; (*timeout) != 0; (*timeout)--) {
    if (onepin_get_pin()) {
      return true;
    }
  }
  return (bool)onepin_get_pin();
}


bool wait_1wire_low(uint32_t *timeout) {
  for (; (*timeout) != 0; (*timeout)--) {
    if (!onepin_get_pin()) {
      return true;
    }
  }
  return (onepin_get_pin() == 0);
}

bool send_1wire_reset(bool sensitive) {
  DBG_LED_RGB3(1, 1, 0);
  onepin_set_mode_tx();
  onepin_set_high();
  busy_loop(RESET_COUNT);
  onepin_set_low();
  if (!sensitive) {
    busy_loop(RESET_COUNT);
    return true;
  }
  uint32_t i = RESET_TIMEOUT;
  onepin_set_mode_rx();
  if (!wait_1wire_high(&i)) {
    RETURN_FALSE;
  }
  i = RESET_RESPONSE_COUNT;
  if (!wait_1wire_low(&i)) {
    RETURN_FALSE;
  };
  if (i > RESET_RESPONSE_COUNT - RESET_RESPONSE_COUNT / 4) {
    RETURN_FALSE;
  };
  onepin_set_mode_tx();
  return true;
}

// Set rx mode and wait for reset from opposite side.
bool accept_1wire_connection(uint32_t timeout) {
  uint32_t start_timeout = timeout;
  DBG_LED_RGB3(0, 1, 1);
  onepin_set_mode_rx();
  if (!wait_1wire_high(&timeout)) {
    RETURN_FALSE;
  }
  // High signal received, verify it (must be long enough)
  timeout = RESET_COUNT * 2;
  if (!wait_1wire_low(&timeout) ||
      timeout > start_timeout - RESET_COUNT / 2) {
    // It wasn't proper reset. Line is broken.
    RETURN_FALSE;
  }
  // Send the reset response.
  onepin_set_mode_tx();
  onepin_set_high();
  busy_loop(RESET_RESPONSE_COUNT);
  onepin_set_low();
  onepin_set_mode_rx();
  return true;
}

void send_1wire_byte(uint8_t byte) {
  uint8_t i;
  //DBG_LED_RGB3(0, 1, 0);
  for (i = 0; i < 8; i++) {
    if (byte & 0x80) {
      onepin_set_high();
      busy_loop(DATA_WAIT_COUNT);
      onepin_set_low();
    } else {
      onepin_set_low();
      busy_loop(DATA_WAIT_COUNT);
      onepin_set_high();
    }
    busy_loop(CLOCK_WAIT_COUNT);
    byte = byte << 1;
  }
}

bool recv_1wire_byte(uint8_t *byte) {
  uint8_t i;
  uint8_t b = 0;
  uint32_t pin = 0;
  uint32_t timeout;
  DBG_LED_RGB3(0, 0, 1);
  for (i = 0; i < 8; i++) {
    timeout = DATA_WAIT_COUNT;
    b = (b << 1);
    // Go to the middle of the current bit.
    busy_loop(CLOCK_WAIT_COUNT * 2);
    pin = onepin_get_pin();
    if (pin) {
      b |= 1;
      // Wait till receiving of the current high signal ended.
      if (!wait_1wire_low(&timeout)) {
        return false;
      }
    } else {
      // Wait till receiving of the current low signal ended.
      if (!wait_1wire_high(&timeout)) {
        return false;
      }
    }
  }
  *byte = b;
  return true;
}



