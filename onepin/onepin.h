/*
  1 wire communication protocol interface.
  LPC1769 and TM4C123 ports.
  This file is part of 'UCSCx, 4357.(010) Embedded Firmware Essentials' project.

  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

// Hardware-dependent portable code.
#if 0
// Template to fill for creating a new hardware port.

#define sys_init() do {  ; } while(0)

// GPIO functionality required to make it work.
#define onepin_init_gpio()  do {  ; } while(0)
#define onepin_set_high()       do {  ; } while(0)
#define onepin_set_low()        do {  ; } while(0)
#define onepin_set_mode_tx()    do {  ; } while(0)
#define onepin_set_mode_rx()    do {  ; } while(0)
#define onepin_get_pin()        ( /* Get pint value as 1 or 0 */ )

// Multiplier to get roughly the same time from CPU ticks count on different boards.
#define ONEPIN_CLOCK_MULTIPLPIER ?
#endif


#ifdef __LPC1768_69
#include "LPC17xx.h"

#define PIN_NUMBER_1WIRE 9
#define PROT_1WIRE       LPC_GPIO0
#define PIN_1WIRE        (1 << PIN_NUMBER_1WIRE)

#define sys_init()

#define onepin_init_gpio()  do { LPC_PINCON->PINSEL0 &= (~(1 << (PIN_NUMBER_1WIRE * 2))); } while(0)
#define onepin_set_high()       do { PROT_1WIRE->FIOSET = PIN_1WIRE; } while(0)
#define onepin_set_low()        do { PROT_1WIRE->FIOCLR = PIN_1WIRE; } while(0)
#define onepin_set_mode_tx()    do { PROT_1WIRE->FIODIR |= PIN_1WIRE; } while(0)
#define onepin_set_mode_rx()    do { PROT_1WIRE->FIODIR &= (~PIN_1WIRE); } while(0)
#define onepin_get_pin()        ((PROT_1WIRE->FIOPIN & PIN_1WIRE) >> PIN_NUMBER_1WIRE)


#define ONEPIN_CLOCK_MULTIPLPIER 100
#endif // __LPC1768_69

#ifdef PART_TM4C123GH6PM
#include "tm4c123gh6pm.h"
#include "hw_memmap.h"
#include "hw_gpio.h"

#include "driverlib/sysctl.h"

// pin is PA2
#define ONEPIN_SYSCTL_RCGC2_BIT  SYSCTL_RCGC2_GPIOA
#define ONEPIN_BASE              GPIO_PORTA_BASE
#define ONEPIN_PORT              ((volatile uint8_t *)(ONEPIN_BASE))
#define ONEPIN_DIR_R             GPIO_PORTA_DIR_R
#define ONEPIN_GPIO_PIN_NUMBER   2
#define ONEPIN_GPIO_PIN          (1 << ONEPIN_GPIO_PIN_NUMBER)

#define ONEPIN_GPIO_DATA_R       (*(volatile uint32_t *)(GPIO_PORTA_DATA_BITS_R + ONEPIN_GPIO_PIN/*((ONEPIN_GPIO_PIN << 2))*/ ))

//80MHz
#define sys_init() do {  ;\
  SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ); \
  uint32_t clock = SysCtlClockGet(); \
} while(0)


static inline void onepin_init_gpio()  {
  SYSCTL_RCGC2_R |= ONEPIN_SYSCTL_RCGC2_BIT;
  while (!(SYSCTL_RCGC2_R & ONEPIN_SYSCTL_RCGC2_BIT)) {}
  ONEPIN_PORT[GPIO_O_CR] = 0xFF;
  ONEPIN_PORT[GPIO_O_AMSEL] &= ~(ONEPIN_GPIO_PIN);
  ONEPIN_PORT[GPIO_O_PCTL] &= ~(ONEPIN_GPIO_PIN);
  ONEPIN_PORT[GPIO_O_AFSEL] &= ~(ONEPIN_GPIO_PIN);
  ONEPIN_PORT[GPIO_O_DEN] |= ONEPIN_GPIO_PIN;
}

#define onepin_set_high()       do { ONEPIN_GPIO_DATA_R = ONEPIN_GPIO_PIN; } while(0)
#define onepin_set_low()        do { ONEPIN_GPIO_DATA_R = 0; } while(0)

#define onepin_set_mode_tx()    do { ONEPIN_DIR_R |= ONEPIN_GPIO_PIN; } while(0)
#define onepin_set_mode_rx()    do { ONEPIN_DIR_R &= ~ONEPIN_GPIO_PIN; } while(0)
#define onepin_get_pin()        ( ONEPIN_GPIO_DATA_R >> ONEPIN_GPIO_PIN_NUMBER )


#define ONEPIN_CLOCK_MULTIPLPIER 80
#endif  // PART_TM4C123GH6PM


#define RETURN_FALSE       do { DBG_LED_RGB3(1, 0, 0); return false; } while (0)

#define ONEPIN_CLOCK_VALUE(v) (v * ONEPIN_CLOCK_MULTIPLPIER)

#define RESET_COUNT           ONEPIN_CLOCK_VALUE(500)
#define RESET_RESPONSE_COUNT  ONEPIN_CLOCK_VALUE(30)
#define RESET_TIMEOUT         ONEPIN_CLOCK_VALUE(60)
#define DATA_WAIT_COUNT       ONEPIN_CLOCK_VALUE(90)
#define CLOCK_WAIT_COUNT      ONEPIN_CLOCK_VALUE(10)



bool wait_1wire_low(uint32_t *timeout);
bool send_1wire_reset(bool sensitive);
bool accept_1wire_connection(uint32_t timeout);
void send_1wire_byte(uint8_t byte);
bool recv_1wire_byte(uint8_t *byte);

