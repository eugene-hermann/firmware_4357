/*
  3 color LED driver
  This file is part of 'UCSCx, 4357.(010) Embedded Firmware Essentials' project.

  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/
#include <stdint.h>
// Color depth up to 24 bit RGB can be implemented by configuring LED bits as PWM.

// RGB 3 bit color functions.
void led_init3(void);

#define RGB3(r, g, b)  (r | (b << 1) | (g << 2))

void led_rgb3(uint8_t rgb);

#ifdef DEBUG
#define DBG_LED_RGB3(r, g, b) led_rgb3(RGB3(r, g, b))

#else

#define DBG_LED_RGB3(r, g, b)
#endif

