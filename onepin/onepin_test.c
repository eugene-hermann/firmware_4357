/*
  1 wire communication protocol.
  Testing.
  This file is part of 'UCSCx, 4357.(010) Embedded Firmware Essentials' project.

  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "led.h"
#include "onepin.h"


int test_wait_low(void) {
  uint32_t timeout = RESET_COUNT * 2;
  int i;
  int result = 0;
  onepin_set_mode_rx();
  for(i = 0 ; i < 100; i++) {
    if (wait_1wire_low(&timeout)) {
      result++;
    }
  }
  DBG_LED_RGB3(0, 0, 0);
  return 100 - result;
}


int test_reset(void) {
  int i;
  int result = 0;
  for(i = 0 ; i < 100; i++) {
    if (send_1wire_reset(true)) {
      DBG_LED_RGB3(1, 1, 1);
      result++;
    }
  }
  return 100 - result;
}

int test_accept(void) {
  int i;
  int result = 0;
  for(i = 0 ; i < 100; i++) {
    if (accept_1wire_connection(1000000)) {
      result++;
      DBG_LED_RGB3(1, 1, 1);
    }
  }
  return 100 - result;
}

void test_send(void) {
  onepin_set_mode_tx();
  for( ;; ) {
    send_1wire_byte(0x33);
  }
}

int test_recv(void) {
  int i;
  int result = 0;
  uint8_t buff[100] = { 0 };
  onepin_set_mode_rx();
  for(i = 0 ; i < 100; i++) {
    if (recv_1wire_byte(buff + i)) {
      DBG_LED_RGB3(1, 1, 1);
      result++;
    }
  }
  return 100 - result;
}

void test_integration_send(void) {
  int i;
  while (!send_1wire_reset(true)) {}
  DBG_LED_RGB3(0, 1, 0);
  for (i = 0; i < 100; i++) {
    send_1wire_byte(0x33);
  }
}

void test_inteegration_recv(void) {
  int i;
  uint8_t buff[100];
  while (!accept_1wire_connection(1000000)) {}
  DBG_LED_RGB3(0, 0, 1);
  for (i = 0; i < 100; i++) {
    recv_1wire_byte(buff + i);
  }
}


#ifdef __LPC1768_69

void test_port(void) {
//  test_wait_low();
//  test_accept();
//    test_reset();
//  test_send();
//    test_recv();
  test_inteegration_recv();
}

#endif // __LPC1768_69

#ifdef PART_TM4C123GH6PM

void test_port(void) {
//  test_wait_low();
//  test_accept();
//    test_reset();
//  test_send();
//  test_recv();
  test_integration_send();
}

#endif

int main( void ) {
  sys_init();
  onepin_init_gpio();
  led_init3();
  test_port();
  DBG_LED_RGB3(0, 0, 0);
  for ( ;; ) {}
}
