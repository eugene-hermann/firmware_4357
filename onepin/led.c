/*
  3 color LED driver
  LPC1769 and TM4C123 ports.
  This file is part of 'UCSCx, 4357.(010) Embedded Firmware Essentials' project.

  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#include "led.h"


#define RED3(rgb)   ((rgb & 1) == 1)
#define GREEN3(rgb) ((rgb & 4) == 4)
#define BLUE3(rgb)  ((rgb & 2) == 2)

#ifdef __LPC1768_69
#include "LPC17xx.h"

void led_init3(void) {
  // P0.22-RED_LED, P3.25-GREEN_LED, P3.26-BLUE_LED
  LPC_PINCON->PINSEL1 &= ~(3 << 12);
  LPC_PINCON->PINSEL7 &= ~(0xf << 18);
  LPC_GPIO0->FIODIR |= (1 << 22);
  LPC_GPIO3->FIODIR |= (3 << 25);
  led_rgb3(RGB3(0, 0, 0));
}

void led_rgb3(uint8_t rgb) {
  if (RED3(rgb)) {
    LPC_GPIO0->FIOCLR = (1 << 22);
  } else {
    LPC_GPIO0->FIOSET = (1 << 22);
  }
  if (GREEN3(rgb)) {
    LPC_GPIO3->FIOCLR = (1 << 25);
  } else {
    LPC_GPIO3->FIOSET = (1 << 25);
  }
  if (BLUE3(rgb)) {
    LPC_GPIO3->FIOCLR = (1 << 26);
  } else {
    LPC_GPIO3->FIOSET = (1 << 26);
  }
}

#endif // __LPC1768_69

#ifdef PART_TM4C123GH6PM
#include <stdbool.h>
#include "tm4c123gh6pm.h"
#include "driverlib/sysctl.h"

#define LED_PORT (*(volatile uint32_t *)(GPIO_PORTF_DATA_BITS_R + 0x0E/*((ONEPIN_GPIO_PIN << 2))*/ ))

void led_init3(void) {
  SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPIOF;
  while (!(SYSCTL_RCGC2_R & SYSCTL_RCGC2_GPIOF)) {};
  GPIO_PORTF_LOCK_R = 0x4C4F434BU;
  GPIO_PORTF_CR_R = 0xFF;
  GPIO_PORTF_AMSEL_R = 0;
  GPIO_PORTF_PCTL_R  = 0;
  GPIO_PORTF_AFSEL_R = 0;
  GPIO_PORTF_DIR_R |= 0x0E;
  GPIO_PORTF_DEN_R = 0xFF;
}


void led_rgb3(uint8_t rgb) {
  LED_PORT = (uint32_t)rgb << 1;
}

#endif // PART_TM4C123GH6PM

void test_led(void) {
  volatile uint32_t i;
  uint8_t rgb;
  led_init3();
  led_rgb3(RGB3(1, 0, 0));
  led_rgb3(RGB3(0, 1, 0));
  led_rgb3(RGB3(0, 0, 1));
  while(1) {
    for (rgb = 0; rgb < 8; rgb++) {
      led_rgb3(rgb);
      for (i = 0; i < 1000000; i++) {};
    }
  }
}
